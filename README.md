# README: Configuration Management with Puppet and Terraform

## Objective

The objective of this assignment is to automate the configuration management of infrastructure resources provisioned using Terraform using Puppet.

## Requirements

### 1. Setup GitLab Repository

- Create a new GitLab repository named "PuppetTerraformIntegration."
- Initialize the repository with a simple README.md file.

### 2. Infrastructure Provisioning with Terraform

- Write Terraform code to provision a simple infrastructure component (e.g., AWS EC2 instance, Azure VM, etc.).
- Store the Terraform configuration files in a directory named "terraform."

### 3. Configuration Management with Puppet

- Write Puppet manifests to configure the provisioned infrastructure component.
- The Puppet manifests should include tasks such as:
  - Installing required packages or software.
  - Configuring system settings or files.
  - Starting or enabling services.
- Store the Puppet manifests and modules in a directory named "puppet."

### 4. Integration

- Create a mechanism to automatically trigger Puppet configuration management after Terraform provisioning.
- This can be achieved using one of the following methods:
  - Use Terraform provisioners to trigger Puppet runs on provisioned instances.
  - Utilize a configuration management tool like Ansible or Shell scripts within Terraform to execute Puppet runs remotely on provisioned instances.

## Prerequisites

- Terraform installed on your local machine.
- Access to the cloud provider (e.g., AWS, Azure) where the infrastructure will be provisioned.
- Proper authentication and access credentials for the cloud provider.

## Steps

1. Clone this repository to your local machine.
2. Navigate to the appropriate directory containing the Terraform code.
3. Initialize the Terraform working directory by running `terraform init`.
4. Review the execution plan by running `terraform plan`.
5. Apply the changes by running `terraform apply`.
6. Meanwhile, paste the `site.pp` file under the `manifests` directory of the puppet server.


## Terraform Configuration (main.tf)

```hcl
provider "aws" {
  region = "us-east-1"
}

# Use existing VPC
data "aws_vpc" "existing_vpc" {
  id = "vpc-03617988af3a2c6fc"
}

# Use existing subnet
data "aws_subnet" "existing_subnet" {
  vpc_id = data.aws_vpc.existing_vpc.id
  id     = "subnet-0ac5282520e4a28e5"
}

# Use existing security group
data "aws_security_group" "existing_security_group" {
  id = "sg-0034dbdada48a57d6"
}

resource "aws_instance" "puppetMaster" {
  ami                         = "ami-07d9b9ddc6cd8dd30"
  instance_type               = "t2.medium"
  subnet_id                   = data.aws_subnet.existing_subnet.id
  key_name                    = "puppetTerraform"
  associate_public_ip_address = true
  security_groups             = [data.aws_security_group.existing_security_group.id] // Attach security group to instance

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("D:\\DEV\\WDTraining\\AWSKeys\\puppetTerraform.pem")
    host        = aws_instance.puppetMaster.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver"
    ]
  }

  tags = {
    Name = "puppetMaster"
  }
}
```

## Puppet Manifest (site.pp)

```puppet
# This file will be used in Puppet-server's manifest directory

package { 'apache2':
  ensure => present,
}

service { 'apache2':
  ensure => running,
  enable => true,
}
```

Follow these steps, and you'll have a complete setup for automating configuration management with Puppet and Terraform.
