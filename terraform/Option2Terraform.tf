provider "aws" {
  region = "us-east-1"
}

# Create a new VPC
resource "aws_vpc" "example_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "example_vpc"
  }
}

# Create a new subnet in the new VPC
resource "aws_subnet" "example_subnet" {
  vpc_id                  = aws_vpc.example_vpc.id
  cidr_block              = "10.0.0.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "example_subnet"
  }
}

# Create a new security group allowing SSH access from Windows
resource "aws_security_group" "example_security_group" {
  vpc_id = aws_vpc.example_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "example_security_group"
  }
}

resource "aws_instance" "puppetMaster" {
  ami                         = "ami-07d9b9ddc6cd8dd30"
  instance_type               = "t2.medium"
  subnet_id                   = aws_subnet.example_subnet.id
  key_name                    = "puppetTerraform"
  associate_public_ip_address = true
  security_groups             = [aws_security_group.example_security_group.id] // Attach security group to instance

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("D:\\DEV\\WDTraining\\AWSKeys\\puppetTerraform.pem")
    host        = aws_instance.puppetMaster.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver"
    ]
  }

  tags = {
    Name = "puppetMaster"
  }
}
